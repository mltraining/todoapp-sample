package ee.bcs.todo;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.stream.Collectors;

public class AppController {

    public VBox mainBox;
    public VBox todoListBox;
    public Label appCaptionLabel;
    public Separator separator;
    public HBox itemAddBox;
    public Button itemAddButton;
    public TextField itemAddTextField;

    public void initialize() {
        todoListBox.getChildren().addAll(DataService.getTodos().stream().map(Item::new).collect(Collectors.toList()));
    }

    public void handleItemAdd(ActionEvent actionEvent) {
        todoListBox.getChildren().add(
                new Item(new DataService.TodoEntity(DataService.TodoEntityType.UNDONE, itemAddTextField.getText())));
        itemAddTextField.clear();
    }

}
