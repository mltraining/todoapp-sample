package ee.bcs.todo;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class Item extends HBox {

    private static final String TODO_BACKGROUND_UNDONE = "-fx-background-color: white;";
    private static final String TODO_BACKGROUND_DONE = "-fx-background-color: lightgreen;";

    public TextField itemTextField;
    public Button itemToggleButton;
    public Button itemDeleteButton;

    private DataService.TodoEntity todo;

    public Item(DataService.TodoEntity todo) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Item.fxml"));
            loader.setController(this);
            this.getChildren().add(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.todo = todo;
        itemTextField.setText(todo.getText());
        if (todo.getType() == DataService.TodoEntityType.DONE) {
            markTodoItemDone();
        } else {
            markTodoItemUndone();
        }

        itemToggleButton.setOnAction(e -> this.toggleTodoItem());
        itemDeleteButton.setOnAction(e -> this.deleteItem());
    }

    private void markTodoItemDone() {
        itemTextField.setStyle(TODO_BACKGROUND_DONE);
        itemToggleButton.setText("Mark undone");
    }

    private void markTodoItemUndone() {
        itemTextField.setStyle(TODO_BACKGROUND_UNDONE);
        itemToggleButton.setText("Mark done");
    }

    private void toggleTodoItem() {
        if (TODO_BACKGROUND_UNDONE.equals(itemTextField.getStyle())) {
            markTodoItemDone();
        } else {
            markTodoItemUndone();
        }
    }

    private void deleteItem() {
        ((VBox)this.getParent()).getChildren().removeIf(n -> n.hashCode() == this.hashCode());
    }
}
