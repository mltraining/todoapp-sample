package ee.bcs.todo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DataService {
    private static List<String> todos = new ArrayList<>(
            Arrays.asList(
                    "UNDONE:Mine poodi: osta saia", "UNDONE:Tee trenni", "UNDONE:Vii prügi välja",
                    "UNDONE:Korista tuba ära", "UNDONE:Loe raamatut", "UNDONE:Vasta e-kirjadele",
                    "UNDONE:Suhtle naabritega", "DONE:Mõtle välja uus anekdoot", "DONE:Mängi lastega",
                    "UNDONE:Valmista õhtusöök", "UNDONE:Värvi lagi ära", "UNDONE:Anna koerale süüa",
                    "UNDONE:Puhasta akvaarium ära", "UNDONE:Vaata Aktuaalset kaamerat", "DONE:Registreeri võistlusele",
                    "UNDONE:Lae telefoni aku ära", "UNDONE:Uuenda viirusetõrjet", "UNDONE:Pese pesu ära",
                    "UNDONE:Kirjuta essee", "UNDONE:Pane ruubiku kuubik kokku", "UNDONE:Pühi koridor puhtaks"));

    public static List<TodoEntity> getTodos() {
        return todos.stream().map(TodoEntity::new).collect(Collectors.toList());
    }

    public static class TodoEntity {
        private TodoEntityType type;
        private String text;

        public TodoEntity(TodoEntityType type, String text) {
            this.type = type;
            this.text = text;
        }

        public TodoEntity(String todoDefinition) {
            String[] definitionParts = todoDefinition.split(":", 2);
            this.type = TodoEntityType.valueOf(definitionParts[0]);
            this.text = definitionParts[1];
        }

        public TodoEntityType getType() {
            return type;
        }

        public String getText() {
            return text;
        }

        @Override
        public String toString() {
            return String.format("%s:%s", this.type, this.text);
        }
    }

    public enum TodoEntityType {
        UNDONE, DONE
    }
}
