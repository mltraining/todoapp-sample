module ee.bcs.todo {
    requires javafx.controls;
    requires javafx.fxml;

    opens ee.bcs.todo to javafx.fxml;
    exports ee.bcs.todo;
}
